package com.heapsteep.controller;

import java.util.ArrayList;
import java.util.List;

//import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.heapsteep.model.Book;
import com.heapsteep.repository.BookRepository;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@RestController
@RequestMapping("/api")
public class BookController {
	@Autowired
    BookRepository bookRepository;

    @GetMapping("/books")
    //@CircuitBreaker(name="my-circuit-breaker-2", fallbackMethod="method2")
    public List<Book> getAllNotes() {
    	//return bookRepository.findAll();
    	new BookController().callDB();
    	
    	return null; 
    }
    
    public List<Book> method2(Exception e) {
    	//Book book=new Book(1L,1,"There is an db error");
    	List list=new ArrayList();
    	list.add("Ther is an db error");
		return list ;
	}
    
    public List<Book> callDB(){
    	System.out.println("-->here calling DB");
    	List<Book> book=bookRepository.findAll();
    	System.out.println(book);
    	return book;
    }


}